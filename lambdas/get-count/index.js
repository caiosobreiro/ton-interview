const countapi = require('countapi-js');

exports.handler = async (event) => {

    const namespace = 'ton-caio-sobreiro'
    const key = 'visits'
    const countData = await countapi.get(namespace, key)

    return { visits: countData.value };
};
