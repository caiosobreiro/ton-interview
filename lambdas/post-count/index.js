const countapi = require('countapi-js');

exports.handler = async (event) => {

    const namespace = 'ton-caio-sobreiro'
    const key = 'visits'
    const countData = await countapi.hit(namespace, key)

    return { visits: countData.value };
};
