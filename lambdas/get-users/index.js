const AWS = require('aws-sdk')

exports.handler = async (event) => {
  
  // Create the DynamoDB service object
  const documentClient = new AWS.DynamoDB.DocumentClient()
  
  const cpf = event.pathParameters && event.pathParameters.cpf ||
    event.queryStringParameters && event.queryStringParameters.cpf
    
  if (!cpf) {
    return {
      statusCode: 400,
      body: JSON.stringify({
        message: 'CPF not provided'
      })
    }
  }

  const params = {
    TableName: 'ton-api-users',
    Key: { cpf }
  }
  
  // Call DynamoDB to retrieve the item from the table
  const user = await documentClient.get(params).promise()
  
  
  return {
    statusCode: Object.keys(user).length ? 200 : 404,
    body: JSON.stringify(user.Item)
  }
}
