const AWS = require('aws-sdk')

exports.handler = async (event) => {
    
    // Set the region 
    AWS.config.update({ region: 'sa-east-1' })
    
    // Create the DynamoDB service object
    var documentClient = new AWS.DynamoDB.DocumentClient()
    
    var params = {
      TableName: 'ton-api-users',
      Item: event
    }
    
    // Call DynamoDB to add the item to the table
    await documentClient.put(params).promise()
    
    return {
      message: "Success"
    }
}
