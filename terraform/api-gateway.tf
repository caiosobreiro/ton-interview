resource "aws_api_gateway_rest_api" "terraform_ton-api" {
    name              = "terraform_ton-api"
    description       = "Technical interview TON - Stone"
    api_key_source    = "HEADER"

    endpoint_configuration {
        types         = [ "REGIONAL" ]
    }
}

/*
    RESOURCES
 */
resource "aws_api_gateway_resource" "user" {
    rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
    parent_id   = aws_api_gateway_rest_api.terraform_ton-api.root_resource_id
    path_part   = "user"
}

resource "aws_api_gateway_resource" "count" {
    rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
    parent_id   = aws_api_gateway_rest_api.terraform_ton-api.root_resource_id
    path_part   = "count"
}

resource "aws_api_gateway_resource" "cpf" {
    rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
    parent_id   = aws_api_gateway_resource.user.id
    path_part   = "{cpf+}"
}

/*
    METHODS
 */
resource "aws_api_gateway_method" "get-count" {
  rest_api_id      = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id      = aws_api_gateway_resource.count.id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_method" "post-count" {
  rest_api_id      = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id      = aws_api_gateway_resource.count.id
  http_method      = "POST"
  authorization    = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_method" "get-user" {
  rest_api_id      = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id      = aws_api_gateway_resource.user.id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_method" "post-user" {
  rest_api_id      = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id      = aws_api_gateway_resource.user.id
  http_method      = "POST"
  authorization    = "NONE"
  api_key_required = true
}

resource "aws_api_gateway_method" "get-user-by-cpf" {
  rest_api_id      = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id      = aws_api_gateway_resource.cpf.id
  http_method      = "ANY"
  authorization    = "NONE"
  api_key_required = true
}

/*
    INTEGRATIONS
 */
resource "aws_api_gateway_integration" "integration-get-count" {
  rest_api_id             = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id             = aws_api_gateway_resource.count.id
  http_method             = aws_api_gateway_method.get-count.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.get-count.invoke_arn
}

resource "aws_api_gateway_integration" "integration-post-count" {
  rest_api_id             = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id             = aws_api_gateway_resource.count.id
  http_method             = aws_api_gateway_method.post-count.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.post-count.invoke_arn
}

resource "aws_api_gateway_integration" "integration-get-user" {
  rest_api_id             = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id             = aws_api_gateway_resource.user.id
  http_method             = aws_api_gateway_method.get-user.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.get-user.invoke_arn
}

resource "aws_api_gateway_integration" "integration-post-user" {
  rest_api_id             = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id             = aws_api_gateway_resource.user.id
  http_method             = aws_api_gateway_method.post-user.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.post-user.invoke_arn
}

resource "aws_api_gateway_integration" "integration-get-user-by-cpf" {
  rest_api_id             = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id             = aws_api_gateway_resource.cpf.id
  http_method             = aws_api_gateway_method.get-user-by-cpf.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.get-user.invoke_arn
}

resource "aws_api_gateway_method_response" "response_200-get-count" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.count.id
  http_method = aws_api_gateway_method.get-count.http_method
  status_code = "200"
}

resource "aws_api_gateway_method_response" "response_200-post-count" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.count.id
  http_method = aws_api_gateway_method.post-count.http_method
  status_code = "200"
}

resource "aws_api_gateway_method_response" "response_200-get-user" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.user.id
  http_method = aws_api_gateway_method.get-user.http_method
  status_code = "200"
}

resource "aws_api_gateway_method_response" "response_200-post-user" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.user.id
  http_method = aws_api_gateway_method.post-user.http_method
  status_code = "200"
}

resource "aws_api_gateway_method_response" "response_200-get-user-by-cpf" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.cpf.id
  http_method = aws_api_gateway_method.get-user-by-cpf.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "IntegrationResponse-get-count" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.count.id
  http_method = aws_api_gateway_method.get-count.http_method
  status_code = aws_api_gateway_method_response.response_200-get-count.status_code

  depends_on = [
    aws_api_gateway_integration.integration-get-user
  ]
}

resource "aws_api_gateway_integration_response" "IntegrationResponse-post-count" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.count.id
  http_method = aws_api_gateway_method.post-count.http_method
  status_code = aws_api_gateway_method_response.response_200-post-count.status_code
}

resource "aws_api_gateway_integration_response" "IntegrationResponse-get-user" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.user.id
  http_method = aws_api_gateway_method.get-user.http_method
  status_code = aws_api_gateway_method_response.response_200-get-user.status_code
}

resource "aws_api_gateway_integration_response" "IntegrationResponse-post-user" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.user.id
  http_method = aws_api_gateway_method.post-user.http_method
  status_code = aws_api_gateway_method_response.response_200-post-user.status_code
}

resource "aws_api_gateway_integration_response" "IntegrationResponse-get-user-by-cpf" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id
  resource_id = aws_api_gateway_resource.cpf.id
  http_method = aws_api_gateway_method.get-user-by-cpf.http_method
  status_code = aws_api_gateway_method_response.response_200-get-user-by-cpf.status_code
}

/*
  STAGE
*/
resource "aws_api_gateway_stage" "dev" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.terraform_ton-api.id
  stage_name    = "dev"
}

/*
  DEPLOYMENT
*/
resource "aws_api_gateway_deployment" "deployment" {
  rest_api_id = aws_api_gateway_rest_api.terraform_ton-api.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_rest_api.terraform_ton-api.body,
      aws_api_gateway_resource.user.id,
      aws_api_gateway_resource.count.id,
      aws_api_gateway_resource.cpf.id,
      aws_api_gateway_method.get-user.id,
      aws_api_gateway_method.get-count.id,
      aws_api_gateway_method.post-user.id,
      aws_api_gateway_method.post-count.id,
      aws_api_gateway_method.get-user-by-cpf.id
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

/*
  USAGE PLAN and KEYS
*/
resource "aws_api_gateway_usage_plan" "basic_plan" {
  name = "basic_plan"

  api_stages {
    api_id = aws_api_gateway_rest_api.terraform_ton-api.id
    stage  = aws_api_gateway_stage.dev.stage_name
  }
}

resource "aws_api_gateway_api_key" "mykey" {
  name = "caio"
}

resource "aws_api_gateway_usage_plan_key" "main" {
  key_id        = aws_api_gateway_api_key.mykey.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.basic_plan.id
}