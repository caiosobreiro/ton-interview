data "archive_file" "get-count" {
  type        = "zip"
  output_path = "${local.lambdas_directory}/get-count/get-count.zip"

  source {
    content  = "${file("${local.lambdas_directory}/get-count/index.js")}"
    filename = "index.js"
  }
}

data "archive_file" "post-count" {
  type        = "zip"
  output_path = "${local.lambdas_directory}/post-count/post-count.zip"

  source {
    content  = "${file("${local.lambdas_directory}/post-count/index.js")}"
    filename = "index.js"
  }
}

data "archive_file" "get-user" {
  type        = "zip"
  output_path = "${local.lambdas_directory}/get-users/get-users.zip"

  source {
    content  = "${file("${local.lambdas_directory}/get-users/index.js")}"
    filename = "index.js"
  }
}

data "archive_file" "post-user" {
  type        = "zip"
  output_path = "${local.lambdas_directory}/post-users/post-users.zip"

  source {
    content  = "${file("${local.lambdas_directory}/post-users/index.js")}"
    filename = "index.js"
  }
}