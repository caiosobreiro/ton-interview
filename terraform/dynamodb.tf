resource "aws_dynamodb_table" "ton-dynamodb-table" {
  name           = "ton-api-users"
  billing_mode   = "PAY_PER_REQUEST"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "cpf"

  attribute {
    name = "cpf"
    type = "S"
  }
}