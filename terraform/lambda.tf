resource "aws_lambda_permission" "lambda_permission_get_count" {
  statement_id  = "AllowApiInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "get-count"
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.terraform_ton-api.execution_arn}/*/*/*"
}

resource "aws_lambda_permission" "lambda_permission_post_count" {
  statement_id  = "AllowApiInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "post-count"
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.terraform_ton-api.execution_arn}/*/*/*"
}

resource "aws_lambda_permission" "lambda_permission_get_user" {
  statement_id  = "AllowApiInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "get-user"
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.terraform_ton-api.execution_arn}/*/*/*"
}

resource "aws_lambda_permission" "lambda_permission_post_user" {
  statement_id  = "AllowApiInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "post-user"
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.terraform_ton-api.execution_arn}/*/*/*"
}

resource "aws_lambda_layer_version" "countapi-js-layer" {
  filename    = "${local.layer_path}/../../../dist/${local.layer_name}.zip"
  layer_name  = "${local.layer_name}"
  description = "countapi-js: 1.0.2"

  compatible_runtimes = ["nodejs14.x"]
}

resource "aws_lambda_function" "get-count" {
  function_name = "get-count"
  handler       = "index.handler"
  runtime       = "${local.runtime}"
  role          = "${aws_iam_role.terraform_function_role.arn}"
  layers        = ["${aws_lambda_layer_version.countapi-js-layer.arn}"]

  filename         = "${data.archive_file.get-count.output_path}"
  source_code_hash = "${data.archive_file.get-count.output_base64sha256}"

  timeout     = 30
  memory_size = 128
}

resource "aws_lambda_function" "post-count" {
  function_name = "post-count"
  handler       = "index.handler"
  runtime       = "${local.runtime}"
  role          = "${aws_iam_role.terraform_function_role.arn}"
  layers        = ["${aws_lambda_layer_version.countapi-js-layer.arn}"]

  filename         = "${data.archive_file.post-count.output_path}"
  source_code_hash = "${data.archive_file.post-count.output_base64sha256}"

  timeout     = 30
  memory_size = 128
}

resource "aws_lambda_function" "get-user" {
  function_name = "get-user"
  handler       = "index.handler"
  runtime       = "${local.runtime}"
  role          = "${aws_iam_role.terraform_function_role.arn}"

  filename         = "${data.archive_file.get-user.output_path}"
  source_code_hash = "${data.archive_file.get-user.output_base64sha256}"

  timeout     = 30
  memory_size = 128
}

resource "aws_lambda_function" "post-user" {
  function_name = "post-user"
  handler       = "index.handler"
  runtime       = "${local.runtime}"
  role          = "${aws_iam_role.terraform_function_role.arn}"

  filename         = "${data.archive_file.post-user.output_path}"
  source_code_hash = "${data.archive_file.post-user.output_base64sha256}"

  timeout     = 30
  memory_size = 128
}