# Desafio
O desafio é construir uma API que conta o número de acessos ao site do Ton e permitir que um usuário crie uma conta.

Para contar os acessos foi utilizada a API https://countapi.xyz e para criar e consultar um usuário, foi utilizado o banco de dados não-relacional DynamoDB (AWS).
Para a construção da API foi utilizado o serviço API Gateway em conjunto com funções Lambda.

# Instalação
Esse repositório contém um projeto Terraform para facilitar o deploy ao ambiente AWS.

### Pré-requisitos

- Ter uma conta AWS
- Ter a ferramenta AWS cli instalada na máquina (https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-cliv2-mac.html#cliv2-mac-install-gui)
- Ter o terraform instalado seguindo os comandos para Mac (https://learn.hashicorp.com/tutorials/terraform/install-cli):
`brew tap hashicorp/tap`
`brew install hashicorp/tap/terraform`

### Processo de deploy

Após clonar o repositório basta executar os comandos a seguir a partir de repositório `terraform` (`cd terraform`):
- `terraform init` - Fará a instalação das dependencias e inicializará o projeto.
- `terraform plan` - Mostra o que será aplicado ao ambiente.
- `terraform apply` - Executa o deploy.

# Utilização da API

Caso não queira executar o deploy em uma conta AWS, já foi realizado o deploy na minha conta pessoal.

### API Keys

Por segurança, essa API exige o uso de chaves de API geradas pela AWS. A chave deverá ser passada no header `x-api-key`.

### URL

A url de acesso é `https://0p6qmnz916.execute-api.sa-east-1.amazonaws.com/dev` e os endpoints e métodos estão descritos na  documentação OpenAPI (Arquivo na raiz do projeto).

Alternativamente, segue documentação online do Postman: https://documenter.getpostman.com/view/8839537/UVCBBQZf#0b8d32fc-44cb-40a2-86d6-52df7752c512

# Arquitetura

![alt text](diagrama.png "Diagrama")